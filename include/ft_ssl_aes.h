/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl_aes.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 10:51:52 by fle-roy           #+#    #+#             */
/*   Updated: 2020/03/30 17:02:14 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SSL_AES
# define FT_SSL_AES

void aes128_skey_imc(void);
void aes128_load_skey(__uint128_t *keys);
void aes128_save_skey(__uint128_t *keys);
void aes128_gen_skey(__uint128_t *key, __uint128_t *res);
void aes128_encrypt_block(__uint128_t *data);
void aes128_decrypt_block(__uint128_t *data);
void aes128_encrypt_ecb(__uint128_t *start, size_t len);
void aes128_encrypt_cbc(__uint128_t *start, size_t len, __uint128_t *iv);
void aes128_decrypt_ecb(__uint128_t *start, size_t len);
void aes128_decrypt_cbc(__uint128_t *start, size_t len, __uint128_t *iv);
void aes128_reverse_key(__uint128_t *key);

#endif
