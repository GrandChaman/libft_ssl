/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl_pbkdf.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/13 11:43:15 by fle-roy           #+#    #+#             */
/*   Updated: 2020/03/02 00:13:52 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SSL_PBKDF_H
# define FT_SSL_PBKDF_H
# include <stdlib.h>
# include "libft.h"
# include "ft_ssl_hash.h"

typedef struct		s_ft_pbkdf2_ctx
{
	t_hash_algo		algo;
	uint8_t			*buf;
	uint8_t			*pass;
	uint8_t			*salt;
	size_t			blen;
	size_t			plen;
	size_t			slen;
	uint32_t		c;
	char			opt;
}					t_ft_pbkdf2_ctx;
char				*ft_pbkdf2_hex(uint32_t dklen, uint8_t *buf);
uint8_t				*ft_pbkdf2(t_ft_pbkdf2_ctx *ctx, uint32_t dklen);
uint8_t				*ft_pbkdf2_f(t_ft_pbkdf2_ctx *ctx, t_ft_hmac_ctx *prf,
	uint32_t i);
uint8_t				*ft_pbkdf2_u(t_ft_pbkdf2_ctx *ctx, t_ft_hmac_ctx *prf,
	uint32_t i);
void				ft_pbkdf2_set_nonce(uint8_t *buf,
	uint32_t nonce);
char				ft_pbkdf2_set_salt(t_ft_pbkdf2_ctx *ctx,
	uint8_t *salt, size_t len);
char				ft_pbkdf2_set_pass(t_ft_pbkdf2_ctx *ctx,
	uint8_t *pass, size_t len);
t_ft_pbkdf2_ctx		*ft_pbkdf2_init(t_hash_algo	algo, uint32_t c, char opt);
void				*ft_pbkdf2_destroy(t_ft_pbkdf2_ctx *ctx);

#endif
