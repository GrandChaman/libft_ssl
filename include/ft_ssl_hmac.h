/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl_hmac.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/13 11:43:15 by fle-roy           #+#    #+#             */
/*   Updated: 2020/02/29 01:22:56 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SSL_HMAC_H
# define FT_SSL_HMAC_H
# include <stdlib.h>
# include "libft.h"

typedef struct		s_ft_hmac_ctx
{
	t_hash_algo		hash;
	uint8_t			*key;
	uint8_t			*ipad;
	uint8_t			*opad;
	size_t			klen;
}					t_ft_hmac_ctx;

t_ft_hmac_ctx		*ft_hmac_init(t_hash_algo algo, uint8_t *key, size_t klen);
uint8_t				*ft_hmac(t_ft_hmac_ctx *ctx, uint8_t *msg, size_t mlen,
	uint8_t opt);
void				*ft_hmac_destroy(t_ft_hmac_ctx *ctx);


#endif
