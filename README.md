# libft_ssl

This project aims to recreate some functions of the OpenSSL program.

## Installation

```sh
make
```

## Testing

```sh
make test
./ft_ssl_test
```
