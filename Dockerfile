FROM archlinux:latest

RUN pacman -Sy --noconfirm base-devel linux linux-headers
RUN pacman -Sy --noconfirm gdb
RUN pacman -Sy --noconfirm wget
RUN pacman -Sy --noconfirm zsh
RUN pacman -Sy --noconfirm gcc
RUN pacman -Sy --noconfirm git
RUN pacman -Sy --noconfirm clang llvm
RUN pacman -Sy --noconfirm nasm
RUN /usr/sbin/groupadd --system sudo && \
    /usr/sbin/useradd -m --groups sudo user && \
    /usr/sbin/sed -i -e "s/Defaults    requiretty.*/ #Defaults    requiretty/g" /etc/sudoers && \
    /usr/sbin/echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers && \
	/usr/sbin/echo  'Set disable_coredump false' >> /etc/sudo.conf
RUN git clone https://aur.archlinux.org/libcsptr.git  && cd libcsptr && chmod 777 -R . && su user -c "makepkg -si --noconfirm" && cd - && rm -rf libcsptr
RUN git clone https://aur.archlinux.org/criterion.git && cd criterion && chmod 777 -R . && su user -c "makepkg -si --noconfirm" && cd - && rm -rf criterion
RUN wget -P ~ https://git.io/.gdbinit
RUN chsh -s /bin/zsh root
RUN sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
WORKDIR "/app"
CMD ["/bin/zsh"]
