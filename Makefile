# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/01/16 13:28:08 by fle-roy           #+#    #+#              #
#    Updated: 2020/03/19 14:06:42 by fle-roy          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

vpath %.c src
vpath %.c src/hash
vpath %.c src/hash/md5
vpath %.c src/hash/sha1
vpath %.c src/hash/sha2
vpath %.c src/hash/sha2/sha224
vpath %.c src/hash/sha2/sha256
vpath %.c src/hash/sha2/sha384
vpath %.c src/hash/sha2/sha512
vpath %.c src/hash/sha2/sha512_224
vpath %.c src/hash/sha2/sha512_256
vpath %.c src/base64
vpath %.c src/des
vpath %.c src/hmac
vpath %.c src/pbkdf
vpath %.s src/aes
vpath %.c tests

SRC = \
		sha224_256_op.c \
		sha224_256_op2.c \
		sha224_256.c \
		sha256.c \
		sha224.c \
		sha384_512_op.c \
		sha384_512_op2.c \
		sha384_512.c \
		sha512.c \
		sha384.c \
		sha512_224.c \
		sha512_256.c \
		md5_op.c \
		md5.c \
		sha1.c \
		sha1_op.c \
		read_fd.c \
		hash_utils.c \
		hash_utils_long.c \
		utils.c \
		hash_to_string.c \
		hash_block.c \
		hash.c \
		base64_utils.c \
		base64_encode.c \
		base64_decode.c \
		base64_wrapper.c \
		base64_output.c \
		des_utils.c \
		des_keys.c \
		des_block_op.c \
		des_f.c \
		des_pkdf.c \
		des_encrypt.c \
		des_decrypt.c \
		des_crypt.c \
		des_io.c \
		des_opts_utils.c \
		des_block_cipher.c \
		des.c \
		hmac.c \
		pbkdf2.c \
		pbkdf2_utils.c

SRC_ASM = \
		aes_keys.s \
		aes128.s
ifndef LIBFT_PATH
	LIBFT_PATH=./libft
endif
INCLUDE= include
OBJ_DIR=obj
DEP_DIR=dep
VERSION_DEFINE := $(shell git describe master --tags)-$(shell \
	git rev-parse --short HEAD)
CFLAG = -g3 -Wall -Wextra -Werror -I $(INCLUDE) \
	-I $(LIBFT_PATH)/include -DVERSION="\"$(VERSION_DEFINE)\""
CC = clang
ASM = nasm
NASM_FLAGS = -f elf64
ifeq ($(UNAME_P),x86_64)
    NASM_FLAGS += -D AMD64
endif
ifneq ($(filter %86,$(UNAME_P)),)
    NASM_FLAGS += -D IA32
endif
ifneq ($(filter arm%,$(UNAME_P)),)
    NASM_FLAGS += -D ARM
endif
LN = ar
LFLAG = rsc
TEST_CFLAG = -I $(INCLUDE) -I $(LIBFT_PATH)/include
TEST_LFLAGS = -lcriterion
TEST_BIN = ft_ssl_test
TEST_SRC = \
	hash.test.c \
	md5.test.c \
	sha1.test.c \
	sha256.test.c \
	sha224.test.c \
	sha384.test.c \
	sha512.test.c \
	sha512_224.test.c \
	sha512_256.test.c \
	base64.test.c \
	stdin.test.c \
	des.test.c \
	hmac.test.c \
	pbkdf2.test.c \
	aes.test.c
OBJ_ASM = $(SRC_ASM:%.s=$(OBJ_DIR)/%.o)
OBJ = $(SRC:%.c=$(OBJ_DIR)/%.o)
DEP_ASM = $(SRC_ASM:%.s=$(DEP_DIR)/%.d)
DEP = $(SRC:%.c=$(DEP_DIR)/%.d)
OBJ_TEST = $(TEST_SRC:%.test.c=$(OBJ_DIR)/%.test.o)
DEP_TEST = $(TEST_SRC:%.test.c=$(DEP_DIR)/%.test.d)
ifeq ($(DEV),true)
	CFLAG += -g3 -DDEV
	TEST_CFLAG += -g3 -DDEV
endif

ifeq ($(CODE_COVERAGE),true)
	CFLAG += -fprofile-instr-generate -fcoverage-mapping
	TEST_CFLAG += -fprofile-instr-generate -fcoverage-mapping
	TEST_LFLAGS += -fprofile-instr-generate
endif
LIBFT = $(LIBFT_PATH)/bin/libft.a
NAME = libft_ssl.a
NAME_UP = LIBFT_SSL
all: libft $(NAME)
test: libft $(TEST_BIN)
libft:
	@$(MAKE) -C $(LIBFT_PATH)
$(OBJ_DIR)/%.test.o: %.test.c
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mBuilding $<\033[0m"
	@$(CC) $(TEST_CFLAG) -c $< -o $@
$(DEP_DIR)/%.test.d: %.test.c
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mGenerating dependencies - $<\033[0m"
	@$(CC) $(TEST_CFLAG) -MM $^ | sed -e '1s/^/$(OBJ_DIR)\//' > $@
$(OBJ_DIR)/%.o: %.c
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mBuilding $<\033[0m"
	@$(CC) $(CFLAG) -c $< -o $@
$(OBJ_DIR)/%.o: %.s
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mBuilding $<\033[0m"
	@$(ASM) $(NASM_FLAGS) $< -o $@
$(DEP_DIR)/%.d: %.c
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mGenerating dependencies - $<\033[0m"
	@$(CC) $(CFLAG) -MM $^ | sed -e '1s/^/$(OBJ_DIR)\//' > $@
$(TEST_BIN): $(LIBFT) $(NAME) $(OBJ_TEST) $(OBJ_ASM)
	@$(CC) $(TEST_LFLAGS) $(OBJ_TEST) $(NAME) $(LIBFT) -o $(TEST_BIN)
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mTests done!\033[0m\n"
$(NAME): $(LIBFT) $(OBJ) $(OBJ_ASM)
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mLinking...\033[0m"
	@$(LN) $(LFLAG) $(NAME) $(OBJ) $(OBJ_ASM)
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mDone!\033[0m\n"
dclean:
	@rm -f $(DEP)
	@printf "[$(NAME_UP)] \033[1;31mCleaned .d!\033[0m\n"
clean: dclean
	@rm -f $(OBJ) $(OBJ_TEST)
	@printf "[$(NAME_UP)] \033[1;31mCleaned .o!\033[0m\n"
fclean: clean
	@rm -f $(NAME) $(TEST_BIN)
	@printf "[$(NAME_UP)] \033[1;31mCleaned .a!\033[0m\n"
re:
	@$(MAKE) -C $(LIBFT_PATH) re
	@$(MAKE) fclean
	@$(MAKE) all
-include $(DEP)
-include $(DEP_ASM)
-include $(DEP_TEST)
.PHONY: all clean fclean re dclean test libft
