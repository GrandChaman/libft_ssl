/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pbkdf2.test.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/11 10:09:38 by fle-roy           #+#    #+#             */
/*   Updated: 2020/02/29 16:46:03 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"
#include <stdlib.h>
#include <criterion/criterion.h>

char		g_be;


void	routine_pbkdf2_test(t_hash_algo mode, uint8_t *pass, size_t plen,
	uint8_t *salt, size_t slen, char *res_verif, uint32_t c, uint32_t dklen)
{
	g_be = is_be();
	t_ft_pbkdf2_ctx		*ctx;
	uint8_t				*res;

	ctx = ft_pbkdf2_init(mode, c, 0);
	ft_pbkdf2_set_pass(ctx, pass, plen);
	ft_pbkdf2_set_salt(ctx, salt, slen);
	res = ft_pbkdf2(ctx, dklen);
	cr_assert_str_eq((char*)res, res_verif);
	free(res);
}

Test(PBKDF2, SHA1_1)
{
	routine_pbkdf2_test(FT_SHA1,
		(uint8_t*)"password", 8,
		(uint8_t*)"salt", 4,
		"0c60c80f961f0e71f3a9b524af6012062fe037a6",
		1, 20);
}

Test(PBKDF2, SHA1_2)
{
	routine_pbkdf2_test(FT_SHA1,
		(uint8_t*)"password", 8,
		(uint8_t*)"salt", 4,
		"ea6c014dc72d6f8ccd1ed92ace1d41f0d8de8957",
		2, 20);
}

Test(PBKDF2, SHA1_3)
{
	routine_pbkdf2_test(FT_SHA1,
		(uint8_t*)"password", 8,
		(uint8_t*)"salt", 4,
		"4b007901b765489abead49d926f721d065a429c1",
		4096, 20);
}

Test(PBKDF2, SHA1_4)
{
	routine_pbkdf2_test(FT_SHA1,
		(uint8_t*)"passwordPASSWORDpassword", 24,
		(uint8_t*)"saltSALTsaltSALTsaltSALTsaltSALTsalt", 36,
		"3d2eec4fe41c849b80c8d83662c0e44a8b291a964cf2f07038",
		4096, 25);
}

Test(PBKDF2, SHA1_5)
{
	routine_pbkdf2_test(FT_SHA1,
		(uint8_t*)"pass\0word", 9,
		(uint8_t*)"sa\0lt", 5,
		"56fa6aa75548099dcc37d7f03425e0c3",
		4096, 16);
}

Test(PBKDF2, SHA256_1)
{
	routine_pbkdf2_test(FT_SHA256,
		(uint8_t*)"password", 8,
		(uint8_t*)"salt", 4,
		"120fb6cffcf8b32c43e7225256c4f837a86548c92ccc35480805987cb70be17b",
		1, 32);
}

Test(PBKDF2, SHA256_2)
{
	routine_pbkdf2_test(FT_SHA256,
		(uint8_t*)"password", 8,
		(uint8_t*)"salt", 4,
		"ae4d0c95af6b46d32d0adff928f06dd02a303f8ef3c251dfd6e2d85a95474c43",
		2, 32);
}

Test(PBKDF2, SHA256_3)
{
	routine_pbkdf2_test(FT_SHA256,
		(uint8_t*)"password", 8,
		(uint8_t*)"salt", 4,
		"c5e478d59288c841aa530db6845c4c8d962893a001ce4e11a4963873aa98134a",
		4096, 32);
}

Test(PBKDF2, SHA256_4)
{
	routine_pbkdf2_test(FT_SHA256,
		(uint8_t*)"passwordPASSWORDpassword", 24,
		(uint8_t*)"saltSALTsaltSALTsaltSALTsaltSALTsalt", 36,
		"348c89dbcbd32b2f32d814b8116e84cf2b17347ebc1800181c4e2a1fb8dd53e1c635518c7dac47e9",
		4096, 40);
}

Test(PBKDF2, SHA256_5)
{
	routine_pbkdf2_test(FT_SHA256,
		(uint8_t*)"pass\0word", 9,
		(uint8_t*)"sa\0lt", 5,
		"89b69d0516f829893c696226650a8687",
		4096, 16);
}

Test(PBKDF2, SHA512_1)
{
	routine_pbkdf2_test(FT_SHA512,
		(uint8_t*)"password", 8,
		(uint8_t*)"salt", 4,
		"867f70cf1ade02cff3752599a3a53dc4af34c7a669815ae5d513554e1c8cf252c02d470a285a0501bad999bfe943c08f050235d7d68b1da55e63f73b60a57fce",
		1, 64);
}

Test(PBKDF2, SHA512_2)
{
	routine_pbkdf2_test(FT_SHA512,
		(uint8_t*)"password", 8,
		(uint8_t*)"salt", 4,
		"e1d9c16aa681708a45f5c7c4e215ceb66e011a2e9f0040713f18aefdb866d53cf76cab2868a39b9f7840edce4fef5a82be67335c77a6068e04112754f27ccf4e",
		2, 64);
}

Test(PBKDF2, SHA512_3)
{
	routine_pbkdf2_test(FT_SHA512,
		(uint8_t*)"password", 8,
		(uint8_t*)"salt", 4,
		"d197b1b33db0143e018b12f3d1d1479e6cdebdcc97c5c0f87f6902e072f457b5143f30602641b3d55cd335988cb36b84376060ecd532e039b742a239434af2d5",
		4096, 64);
}

Test(PBKDF2, SHA512_4)
{
	routine_pbkdf2_test(FT_SHA512,
		(uint8_t*)"passwordPASSWORDpassword", 24,
		(uint8_t*)"saltSALTsaltSALTsaltSALTsaltSALTsalt", 36,
		"8c0511f4c6e597c6ac6315d8f0362e225f3c501495ba23b868c005174dc4ee71115b59f9e60cd9532fa33e0f75aefe30225c583a186cd82bd4daea9724a3d3b8",
		4096, 64);
}
