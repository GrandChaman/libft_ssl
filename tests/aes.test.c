/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   base64.test.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/11 10:09:38 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/09 18:04:54 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"
#include <stdlib.h>
#include <criterion/criterion.h>
#define HEX_CHARSET "0123456789abcdef"

void print_uint128(__uint128_t n)
{
    if (n == 0) {
      return;
    }

    print_uint128(n/16);
    putchar(HEX_CHARSET[n%16]);
}

Test(AES128_KeyGeneration, TestVector0)
{
	__uint128_t key;
	__uint128_t skeys[11] = {0};

	key = 0;
	aes128_gen_skey(&key, skeys);
	cr_assert(skeys[0] == ft_bigatoi_base("00000000000000000000000000000000", HEX_CHARSET));
	cr_assert(skeys[1] == ft_bigatoi_base("63636362636363626363636263636362", HEX_CHARSET));
	cr_assert(skeys[2] == ft_bigatoi_base("aafbfbf9c998989baafbfbf9c998989b", HEX_CHARSET));
	cr_assert(skeys[3] == ft_bigatoi_base("99ac0f0b3357f4f2facf6c6950349790", HEX_CHARSET));
	cr_assert(skeys[4] == ft_bigatoi_base("2bee917eb2429e7581156a877bda06ee", HEX_CHARSET));
	cr_assert(skeys[5] == ft_bigatoi_base("90924bf3bb7cda8d093e44f8882b2e7f", HEX_CHARSET));
	cr_assert(skeys[6] == ft_bigatoi_base("a79bb46a3709ff998c752514854b61ec", HEX_CHARSET));
	cr_assert(skeys[7] == ft_bigatoi_base("9bf01bc63c6bafac0b62503587177521", HEX_CHARSET));
	cr_assert(skeys[8] == ft_bigatoi_base("9ffa1d51040a06973861a93b3303f90e", HEX_CHARSET));
	cr_assert(skeys[9] == ft_bigatoi_base("4149664cdeb37b1ddab97d8ae2d8d4b1", HEX_CHARSET));
	cr_assert(skeys[10] == ft_bigatoi_base("8e188f6fcf51e92311e2923ecb5befb4", HEX_CHARSET));
}

Test(AES128_KeyGeneration, TestVector1)
{
	__uint128_t key;
	__uint128_t skeys[11] = {0};

	key = -1;
	aes128_gen_skey(&key, skeys);
	cr_assert(skeys[0] == ft_bigatoi_base("ffffffffffffffffffffffffffffffff", HEX_CHARSET));
	cr_assert(skeys[1] == ft_bigatoi_base("16161617e9e9e9e816161617e9e9e9e8", HEX_CHARSET));
	cr_assert(skeys[2] == ft_bigatoi_base("f0474745e65151520fb8b8ba19aeaead", HEX_CHARSET));
	cr_assert(skeys[3] == ft_bigatoi_base("6e8ca0a49ecbe7e1789ab6b377220e09", HEX_CHARSET));
	cr_assert(skeys[4] == ft_bigatoi_base("b6609b17d8ec3bb34627dc523ebd6ae1", HEX_CHARSET));
	cr_assert(skeys[5] == ft_bigatoi_base("e658c61350385d0488d466b7cef3bae5", HEX_CHARSET));
	cr_assert(skeys[6] == ft_bigatoi_base("8dc92dd16b91ebc23ba9b6c6b37dd071", HEX_CHARSET));
	cr_assert(skeys[7] == ft_bigatoi_base("50d17d3cdd1850edb689bb2f8d200de9", HEX_CHARSET));
	cr_assert(skeys[8] == ft_bigatoi_base("5d33a5680de2d854d0fa88b966733396", HEX_CHARSET));
	cr_assert(skeys[9] == ft_bigatoi_base("a314050efe27a066f3c57832233ff08b", HEX_CHARSET));
	cr_assert(skeys[10] == ft_bigatoi_base("26c3d78c85d7d2827bf072e488350ad6", HEX_CHARSET));
}

Test(AES128_KeyGeneration, TestVector2)
{
	__uint128_t key;
	__uint128_t skeys[11] = {0};

	key = ft_bigatoi_base("0f0e0d0c0b0a09080706050403020100", HEX_CHARSET);
	aes128_gen_skey(&key, skeys);
	cr_assert(skeys[0] == ft_bigatoi_base("0f0e0d0c0b0a09080706050403020100", HEX_CHARSET));
	cr_assert(skeys[1] == ft_bigatoi_base("fe76abd6f178a6dafa72afd2fd74aad6", HEX_CHARSET));
	cr_assert(skeys[2] == ft_bigatoi_base("feb3306800c59bbef1bd3d640bcf92b6", HEX_CHARSET));
	cr_assert(skeys[3] == ft_bigatoi_base("41bf6904bf0c596cbfc9c2d24e74ffb6", HEX_CHARSET));
	cr_assert(skeys[4] == ft_bigatoi_base("fd8d05fdbc326cf9033e3595bcf7f747", HEX_CHARSET));
	cr_assert(skeys[5] == ft_bigatoi_base("aa22f6ad57aff350eb9d9fa9e8a3aa3c", HEX_CHARSET));
	cr_assert(skeys[6] == ft_bigatoi_base("6b1fa30ac13d55a79692a6f77d0f395e", HEX_CHARSET));
	cr_assert(skeys[7] == ft_bigatoi_base("26c0a94e4ddf0a448ce25fe31a70f914", HEX_CHARSET));
	cr_assert(skeys[8] == ft_bigatoi_base("d27abfaef4ba16e0b9651ca435874347", HEX_CHARSET));
	cr_assert(skeys[9] == ft_bigatoi_base("4e972cbe9ced9310685785f0d1329954", HEX_CHARSET));
	cr_assert(skeys[10] == ft_bigatoi_base("c5302b4d8ba707f3174a94e37f1d1113", HEX_CHARSET));
}

Test(AES128_KeyGeneration, TestVector3)
{
	__uint128_t key;
	__uint128_t skeys[11] = {0};

	key = ft_bigatoi_base("2a6f746968636e656d2a20a599e22069", HEX_CHARSET);
	aes128_gen_skey(&key, skeys);
	cr_assert(skeys[0] == ft_bigatoi_base("2a6f746968636e656d2a20a599e22069", HEX_CHARSET)); 
	cr_assert(skeys[1] == ft_bigatoi_base("4f21b253654ec63a0d2da85f600788fa", HEX_CHARSET));
	cr_assert(skeys[2] == ft_bigatoi_base("aac1a9f9e5e01baa80aedd908d8375cf", HEX_CHARSET));
	cr_assert(skeys[3] == ft_bigatoi_base("dba062db7161cb229481d088142f0d18", HEX_CHARSET));
	cr_assert(skeys[4] == ft_bigatoi_base("93d694cb4876f61039173d32ad96edba", HEX_CHARSET));
	cr_assert(skeys[5] == ft_bigatoi_base("50fd4461c32bd0aa8b5d26bab24a1b88", HEX_CHARSET));
	cr_assert(skeys[6] == ft_bigatoi_base("4592fdc2156fb9a3d64469095d194fb3", HEX_CHARSET));
	cr_assert(skeys[7] == ft_bigatoi_base("fece2dcfbb5cd00dae3369ae787700a7", HEX_CHARSET));
	cr_assert(skeys[8] == ft_bigatoi_base("196d1f93e7a3325c5cffe251f2cc8bff", HEX_CHARSET));
	cr_assert(skeys[9] == ft_bigatoi_base("8c2978ba9544672972e755752e18b724", HEX_CHARSET));
	cr_assert(skeys[10] == ft_bigatoi_base("b1f658483ddf20f2a89b47dbda7c12ae", HEX_CHARSET));
}

Test(AES128_Encryption, TestVector0)
{
	__uint128_t data;
	__uint128_t key;
	__uint128_t skeys[11] = {0};

	key = 0;
	data = 0;
	aes128_gen_skey(&key, skeys);
	aes128_load_skey(skeys);
	aes128_encrypt_block(&data);
	cr_assert(data == ft_bigatoi_base("2e2b34ca59fa4c883b2c8aefd44be966", HEX_CHARSET));
}

Test(AES128_Encryption, TestVector1)
{
	__uint128_t data;
	__uint128_t key;
	__uint128_t skeys[11] = {0};

	key = -1;
	data = 0;
	aes128_gen_skey(&key, skeys);
	aes128_load_skey(skeys);
	aes128_encrypt_block(&data);
	cr_assert(data == ft_bigatoi_base("2cc9bf3845486489cd5f7d878c25f6a1", HEX_CHARSET));
}

Test(AES128_Encryption, TestVector2)
{
	__uint128_t data;
	__uint128_t key;
	__uint128_t skeys[11] = {0};

	key = ft_bigatoi_base("0f0e0d0c0b0a09080706050403020100", HEX_CHARSET);
	data = 0;
	aes128_gen_skey(&key, skeys);
	aes128_load_skey(skeys);
	aes128_encrypt_block(&data);
	cr_assert(data == ft_bigatoi_base("79d8c8a162814f6f825b8f87373ba1c6", HEX_CHARSET));
}

Test(AES128_Encryption, TestVector3)
{
	__uint128_t data;
	__uint128_t key;
	__uint128_t skeys[11] = {0};

	key = ft_bigatoi_base("2a6f746968636e656d2a20a599e22069", HEX_CHARSET);
	data = 0;
	aes128_gen_skey(&key, skeys);
	aes128_load_skey(skeys);
	aes128_encrypt_block(&data);
	cr_assert(data == ft_bigatoi_base("55a92b81a1897d38d7e87d8bb467ba86", HEX_CHARSET));
}


Test(AES128_Encryption, TestVector4)
{
	__uint128_t data;
	__uint128_t key;
	__uint128_t skeys[11] = {0};

	key = ft_bigatoi_base("2a6f746968636e656d2a20a599e22069", HEX_CHARSET);
	data = ft_bigatoi_base("2a6f746968636e656d2a20a599e22069", HEX_CHARSET);
	aes128_gen_skey(&key, skeys);
	aes128_load_skey(skeys);
	aes128_encrypt_block(&data);
	cr_assert(data == ft_bigatoi_base("a5b7e1424d848688c8f8874aa94f0d47", HEX_CHARSET));
}

Test(AES128_Decryption, TestVector0)
{
	__uint128_t data;
	__uint128_t key;
	__uint128_t skeys[11] = {0};

	key = 0;
	data = 0;
	aes128_gen_skey(&key, skeys);
	aes128_load_skey(skeys);
	aes128_encrypt_block(&data);

	aes128_skey_imc();
	aes128_decrypt_block(&data);
	cr_assert(data == 0);
}

Test(AES128_Decryption, TestVector1)
{
	__uint128_t data;
	__uint128_t key;
	__uint128_t skeys[11] = {0};

	key = -1;
	data = 0;
	aes128_gen_skey(&key, skeys);
	aes128_load_skey(skeys);
	aes128_encrypt_block(&data);
	aes128_skey_imc();
	aes128_decrypt_block(&data);
	cr_assert(data == 0);
}

Test(AES128_Decryption, TestVector2)
{
	__uint128_t data;
	__uint128_t key;
	__uint128_t skeys[11] = {0};

	key = -1;
	data = -1;
	aes128_gen_skey(&key, skeys);
	aes128_load_skey(skeys);
	aes128_encrypt_block(&data);
	aes128_skey_imc();
	aes128_decrypt_block(&data);
	cr_assert(data == -1);
}

Test(AES128_EncryptionECB, TestVector0)
{
	__uint128_t data[3];
	__uint128_t key;
	__uint128_t skeys[11] = {0};

	key = -1;
	data[0] = -1;
	data[1] = -1;
	data[2] = -1;
	aes128_gen_skey(&key, skeys);
	aes128_load_skey(skeys);
	aes128_encrypt_ecb(data, sizeof(__uint128_t) * 3);
	cr_assert(data[0] == ft_bigatoi_base("79b93a19527051b230cf80b27c21bfbc", HEX_CHARSET));
	cr_assert(data[1] == ft_bigatoi_base("79b93a19527051b230cf80b27c21bfbc", HEX_CHARSET));
	cr_assert(data[2] == ft_bigatoi_base("79b93a19527051b230cf80b27c21bfbc", HEX_CHARSET));
}

Test(AES128_EncryptionECB, TestVector1)
{
	__uint128_t data[3];
	__uint128_t key;
	__uint128_t skeys[11] = {0};

	key = -1;
	data[0] = -1;
	data[1] = 0;
	data[2] = -1;
	aes128_gen_skey(&key, skeys);
	aes128_load_skey(skeys);
	aes128_encrypt_ecb(data, sizeof(__uint128_t) * 3);
	cr_assert(data[0] == ft_bigatoi_base("79b93a19527051b230cf80b27c21bfbc", HEX_CHARSET));
	cr_assert(data[1] == ft_bigatoi_base("2cc9bf3845486489cd5f7d878c25f6a1", HEX_CHARSET));
	cr_assert(data[2] == ft_bigatoi_base("79b93a19527051b230cf80b27c21bfbc", HEX_CHARSET));
}

Test(AES128_DecryptionECB, TestVector0)
{
	__uint128_t data[3];
	__uint128_t key;
	__uint128_t skeys[11] = {0};

	key = -1;
	data[0] = -1;
	data[1] = -1;
	data[2] = -1;
	aes128_gen_skey(&key, skeys);
	aes128_load_skey(skeys);
	aes128_encrypt_ecb(data, sizeof(__uint128_t) * 3);
	aes128_skey_imc();
	aes128_decrypt_ecb(data, sizeof(__uint128_t) * 3);
	cr_assert(data[0] == -1);
	cr_assert(data[1] == -1);
	cr_assert(data[2] == -1);
}

Test(AES128_DecryptionECB, TestVector1)
{
	__uint128_t data[3];
	__uint128_t key;
	__uint128_t skeys[11] = {0};

	key = -1;
	data[0] = -1;
	data[1] = 0;
	data[2] = -1;
	aes128_gen_skey(&key, skeys);
	aes128_load_skey(skeys);
	aes128_encrypt_ecb(data, sizeof(__uint128_t) * 3);
	aes128_skey_imc();
	aes128_decrypt_ecb(data, sizeof(__uint128_t) * 3);
	cr_assert(data[0] == -1);
	cr_assert(data[1] == 0);
	cr_assert(data[2] == -1);
}

Test(AES128_EncryptionCBC, TestVector0)
{
	__uint128_t data[3];
	__uint128_t key;
	__uint128_t iv;
	__uint128_t skeys[11] = {0};

	key = 0;
	iv = 0;
	data[0] = 0;
	data[1] = 0;
	data[2] = 0;
	aes128_gen_skey(&key, skeys);
	aes128_load_skey(skeys);
	aes128_encrypt_cbc(data, sizeof(__uint128_t) * 3, &iv);
	cr_assert(data[0] == ft_bigatoi_base("2e2b34ca59fa4c883b2c8aefd44be966", HEX_CHARSET));
	cr_assert(data[1] == ft_bigatoi_base("bc8de920fa13d313d79ee2524abd95f7", HEX_CHARSET));
	cr_assert(data[2] == ft_bigatoi_base("b3bff58dbfb4705340f3dd0f6df60ca1", HEX_CHARSET));
}

Test(AES128_EncryptionCBC, TestVector1)
{
	__uint128_t data[3];
	__uint128_t key;
	__uint128_t iv;
	__uint128_t skeys[11] = {0};

	key = -1;
	iv = -1;
	data[0] = -1;
	data[1] = -1;
	data[2] = -1;
	aes128_gen_skey(&key, skeys);
	aes128_load_skey(skeys);
	aes128_encrypt_cbc(data, sizeof(__uint128_t) * 3, &iv);
	cr_assert(data[0] == ft_bigatoi_base("2cc9bf3845486489cd5f7d878c25f6a1", HEX_CHARSET));
	cr_assert(data[1] == ft_bigatoi_base("51b73666dddb39b3c0125dcb7a52817f", HEX_CHARSET));
	cr_assert(data[2] == ft_bigatoi_base("6ce7234f5010604b2d404836a8298ee7", HEX_CHARSET));
}

Test(AES128_EncryptionCBC, TestVector2)
{
	__uint128_t data[3];
	__uint128_t key;
	__uint128_t iv;
	__uint128_t skeys[11] = {0};

	key = -1;
	iv = ft_bigatoi_base("efcdab8967452301efcdab8967452301", HEX_CHARSET);
	data[0] = iv;
	data[1] = -1;
	data[2] = iv;
	aes128_gen_skey(&key, skeys);
	aes128_load_skey(skeys);
	aes128_encrypt_cbc(data, sizeof(__uint128_t) * 3, &iv);
	cr_assert(data[0] == ft_bigatoi_base("2cc9bf3845486489cd5f7d878c25f6a1", HEX_CHARSET));
	cr_assert(data[1] == ft_bigatoi_base("51b73666dddb39b3c0125dcb7a52817f", HEX_CHARSET));
	cr_assert(data[2] == ft_bigatoi_base("63647cd0fee50558d45ecfb3d54eb1ee", HEX_CHARSET));
}


Test(AES128_DecryptionCBC, TestVector0)
{
	__uint128_t data[3];
	__uint128_t key;
	__uint128_t iv;
	__uint128_t skeys[11] = {0};

	key = 0;
	iv = 0;
	data[0] = 0;
	data[1] = 0;
	data[2] = 0;
	aes128_gen_skey(&key, skeys);
	aes128_load_skey(skeys);
	aes128_encrypt_cbc(data, sizeof(__uint128_t) * 3, &iv);
	aes128_skey_imc();
	aes128_decrypt_cbc(data, sizeof(__uint128_t) * 3, &iv);
	cr_assert(data[0] == 0);
	cr_assert(data[1] == 0);
	cr_assert(data[2] == 0);
}

Test(AES128_DecryptionCBC, TestVector1)
{
	__uint128_t data[3];
	__uint128_t key;
	__uint128_t iv;
	__uint128_t skeys[11] = {0};

	key = -1;
	iv = -1;
	data[0] = -1;
	data[1] = -1;
	data[2] = -1;
	aes128_gen_skey(&key, skeys);
	aes128_load_skey(skeys);
	aes128_encrypt_cbc(data, sizeof(__uint128_t) * 3, &iv);
	aes128_skey_imc();
	aes128_decrypt_cbc(data, sizeof(__uint128_t) * 3, &iv);
	cr_assert(data[0] == -1);
	cr_assert(data[1] == -1);
	cr_assert(data[2] == -1);
}

Test(AES128_DecryptionCBC, TestVector2)
{
	__uint128_t data[3];
	__uint128_t key;
	__uint128_t iv;
	__uint128_t skeys[11] = {0};

	key = -1;
	iv = ft_bigatoi_base("efcdab8967452301efcdab8967452301", HEX_CHARSET);
	data[0] = iv;
	data[1] = -1;
	data[2] = iv;
	aes128_gen_skey(&key, skeys);
	aes128_load_skey(skeys);
	aes128_encrypt_cbc(data, sizeof(__uint128_t) * 3, &iv);
	aes128_skey_imc();
	aes128_decrypt_cbc(data, sizeof(__uint128_t) * 3, &iv);
	cr_assert(data[0] == iv);
	cr_assert(data[1] == -1);
	cr_assert(data[2] == iv);
}

Test(AES128_DecryptionCBC, TestVector3)
{
	__uint128_t data[3];
	__uint128_t key;
	__uint128_t iv;
	__uint128_t skeys[11] = {0};

	key = 0;
	iv = 0;
	data[0] = 0xa32f7dc0773ab2;
	data[1] = 0xa32f7dc0773ab3;
	data[2] = 0xa32f7dc0773ab4;
	aes128_gen_skey(&key, skeys);
	aes128_load_skey(skeys);
	aes128_encrypt_cbc(data, sizeof(__uint128_t) * 3, &iv);
	cr_assert(data[0] != 0xa32f7dc0773ab2);
	cr_assert(data[1] != 0xa32f7dc0773ab3);
	cr_assert(data[2] != 0xa32f7dc0773ab4);
	aes128_load_skey(skeys);
	aes128_skey_imc();
	aes128_decrypt_cbc(data, sizeof(__uint128_t) * 3, &iv);
	cr_assert(data[0] == 0xa32f7dc0773ab2);
	cr_assert(data[1] == 0xa32f7dc0773ab3);
	cr_assert(data[2] == 0xa32f7dc0773ab4);
}
