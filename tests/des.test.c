/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   des.test.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/06 10:57:06 by fle-roy           #+#    #+#             */
/*   Updated: 2019/06/14 13:34:13 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "ft_ssl_des.h"
#include "libft.h"
#include <stdlib.h>
#include <criterion/criterion.h>

Test(DES, CreateSubkeys)
{
	t_ft_num	nb;
	t_ft_num	res[16];

	nb.x64 = 0x133457799BBCDFF1;
	ft_des_get_subkeys(nb, res);
	cr_assert(res[0].x48 == 0x1B02EFFC7072);
	cr_assert(res[1].x48 == 0x79AED9DBC9E5);
	cr_assert(res[2].x48 == 0x55FC8A42CF99);
	cr_assert(res[3].x48 == 0x72ADD6DB351D);
	cr_assert(res[4].x48 == 0x7CEC07EB53A8);
	cr_assert(res[5].x48 == 0x63A53E507B2F);
	cr_assert(res[6].x48 == 0xEC84B7F618BC);
	cr_assert(res[7].x48 == 0xF78A3AC13BFB);
	cr_assert(res[8].x48 == 0xE0DBEBEDE781);
	cr_assert(res[9].x48 == 0xB1F347BA464F);
	cr_assert(res[10].x48 == 0x215FD3DED386);
	cr_assert(res[11].x48 == 0x7571F59467E9);
	cr_assert(res[12].x48 == 0x97C5D1FABA41);
	cr_assert(res[13].x48 == 0x5F43B7F2E73A);
	cr_assert(res[14].x48 == 0xBF918D3D3F0A);
	cr_assert(res[15].x48 == 0xCB3D8B0E17F5);
}

Test(DES, IP)
{
	t_ft_num num;

	num.x64 = 0x123456789ABCDEF;
	ft_des_ip(&num);
	cr_assert(num.x64 == 0xCC00CCFFF0AAF0AA);
}

Test(DES, FP)
{
	t_ft_num num;

	num.x64 = 0xA4CD99543423234;
	ft_des_fp(&num);
	cr_assert(num.x64 == 0x85E813540F0AB405);
}

Test(DES, f_e)
{
	t_ft_num num;

	num.x32 = 0xF0AAF0AA;
	ft_des_f_e(&num);
	cr_assert(num.x48 == 0x7A15557A1555);
}

Test(DES, f_xor)
{
	t_ft_num num;
	t_ft_num key;

	num.x48 = 0x7A15557A1555;
	key.x48 = 0x1B02EFFC7072;
	ft_des_f_xor(&num, key);
	cr_assert(num.x48 == 0x6117BA866527);
}

Test(DES, f_sbox)
{
	t_ft_num num;

	num.x48 = 0x6117BA866527;
	ft_des_f_sbox(&num);
	cr_assert(num.x32 == 0x5C82B597);
}

Test(DES, f_p)
{
	t_ft_num num;

	num.x32 = 0x5C82B597;
	ft_des_f_p(&num);
	cr_assert(num.x32 == 0x234AA9BB);
}

Test(DES, f)
{
	t_ft_num num;
	t_ft_num key;

	num.x32 = 0xF0AAF0AA;
	key.x48 = 0x1B02EFFC7072;
	ft_des_f(&num, key);
	cr_assert(num.x32 == 0x234AA9BB);
}


Test(DES, process_block_enc)
{
	t_ft_num	num;
	t_ft_num	key[16];

	num.x64 = 0x133457799BBCDFF1;
	ft_des_get_subkeys(num, key);
	num.x64 = 0x123456789ABCDEF;
	ft_des_process_block(&num, key, DES_OPT_E);
	cr_assert(num.x64 == 0x85E813540F0AB405);
}

Test(DES, process_block_dec)
{
	t_ft_num	num;
	t_ft_num	key[16];

	num.x64 = 0x133457799BBCDFF1;
	ft_des_get_subkeys(num, key);
	num.x64 = 0x85E813540F0AB405;
	ft_des_process_block(&num, key, DES_OPT_D);
	cr_assert(num.x64 == 0x123456789ABCDEF);
}

Test(DES, str_to_key)
{
	cr_assert(ft_des_str_to_key("Hello").x64 == 0x48656C6C6F000000);
	cr_assert(ft_des_str_to_key("Hello123").x64 == 0x48656C6C6F313233);
	cr_assert(ft_des_str_to_key("Hello123456789").x64 == 0x48656C6C6F313233);
}

Test(DES, pkdf_md5)
{
	t_ft_des des;

	des.salt.x64 = 0x83AACC733B61BFEB;
	ft_des_pkdf(&des, "salut", NULL);
	cr_assert(des.key.x64 == 0x595DA72A7421C9E9);
	des.salt.x64 = 0x83AABB733B61BFEB;
	ft_des_pkdf(&des, "toto", NULL);
	cr_assert(des.key.x64 == 0x3CDA3C5E6A7EF5A4);
}