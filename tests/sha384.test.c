/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha384.test.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/11 10:09:38 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/10 13:00:20 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"
#include <stdlib.h>
#include <criterion/criterion.h>

char		g_be;


void	routine_sha384_test(char *str, char *res_verif)
{
	t_ft_hash_ctx 	ctx;
	char	*res;

	g_be = is_be();
	ctx.hash = g_hash_table[HT_SHA384];
	ft_sha384_init(&ctx);
	ft_hash_block_update(&ctx, (unsigned char*)str,
		ft_strlen((const char*)str));
	res = (char*)ft_hash_block_finish(&ctx);
	cr_assert(!strcmp(res, (const char*)res_verif));
	free(res);
}

Test(SHA384, Simple)
{
	routine_sha384_test("a", "54a59b9f22b0b80880d8427e548b7c23abd873486e1f035dce9cd697e85175033caa88e6d57bc35efae0b5afd3145f31");
	routine_sha384_test("ab", "c7be03ba5bcaa384727076db0018e99248e1a6e8bd1b9ef58a9ec9dd4eeebb3f48b836201221175befa74ddc3d35afdd");
}

Test(SHA384, BigUnder384)
{
	routine_sha384_test("hoKgmq3kcZXGR0jNbh+EPI2fIrcKZlDszuVPATUd0Ds=",
		"ea76f4f78bf27c5f8f3f75eda871053b95643241f2e204d45291cda29898ad02446b477ba1895cc9b04d28ee6a61369e");
	routine_sha384_test("yuEWkKBs0Uf7tv8FwsnEDC+zomXpbGkjHBB0OvsTufdyrxZp9V04vO",
		"c0c892c1b3d666f8b8e0207082ee0d0a5d1b3251b9f548de79ecf3de8c512d19f5741dee67b2b5151b8bbfe391882c2c");
	routine_sha384_test("7U96W6jnIpgiowEIbK3270sYj/uFoqzpJSmhKyeK+bEz3u+JtiOuK5",
		"9ab6e045c0425d77b5b3d615ccfd9267a14a1800009e8664167da313df394ef91eabf47127c61638b0d650bb9c96e098");
}

Test(SHA384, BigAbove384)
{
	routine_sha384_test("KLwFM4Rz2Q4MO2BPR0EIsd6IVlW4kqvJQskLSsCrf/grwGzqWwhgiZre67Nv/jEjiVZKWR1EuCDymSblxtRN0F+VqLJKOkrv9xcPE3pKMD3/OYcrjaJQQW4iGT8uZ2KQDN5szkjWHLK7lx+oJbs5CwPdJbe+4gmumpafjhnvsXBstUkju/DSe/35nYgoB2hTpNf1lYAygaABIjP4tLH8XUDj28LeGj+QCGyztqR2Gbg0Xsj1YI06l0A7Zi0jv0VzOKXGF0drc42mxM3oP3s2BzeCJdzGOPyhvQavqodaWTXt+ab5WzuUiBQZC6ss9hIyiAJxtE6Y4A3Iv5XaFa1lxFUxS37YDbKdyhuV6FF2ql+3u85FrntpINb49Tw9DoCmsJQ2ZQRH4BK1nJGSo4Xl2cs0uUxbjiW4PPzrk0UohmOqvuCcJD5L3C4qI4+NM5hgVLh9IjC4467IB1MbxRISOvw4muqV45f5a8Gmf5uvxSYAz2rsrG03I9CbjIdz3q6X64OuJbUfO5LY2nn7cgFCvTp6dNYZ4/FkN3fPgfH+eCZ6EZIcB/iWoHXG9Ex7UgmhF41KjjUUkPz11ceUBni0Wp7ifhHMigYuyWYsJw7uRZcKXHwQk/9JGljtIxqJ++ODrF9W4LqJ1c0h+7Ww2dHMbEvqCLHXjaJnUENjnM1UpnQSMBrU3cUn6bAlkDxNS8egOcSokkZRIjtZyRM8uzLnt+gebKMt5M2qPtPM5J+ga2pTVMEUI/VI+37oE4NcYcAU9s25W+qGaGKwmKWhHJpbdM26vqUj96Ih3fmHVPUAg9YRWCT6m2g2usa5/GvoOp1vKlDWmutUNsh/6bU6i0cWAE/Fa0J+1yoC48roerRJLy8i9URWN9BoqZcWfJg5D1fOw4HRnj2Ysg9Jg4CAeRstlykcYDvBkA+t8hIsSpkB/ddlY94uFmYPQz20NY8VjKYt8cxIMUIrKrWkAFMn1GwBuwFOhk56Yv1bjHeII9hwCGuUq7PH2chKDA2PxVS4T+CkiYl/xaPMGyUpc4Ef7HVZSZ2rzwARjLb/TKbetwdOue4YLTzJc6ZGFe+m4iaeqsaxsW3uI2m6MdhO9OZbStlAMcig4B2GXfoNHaivNvrdiBfPfnXFELWmdErkHT8/sl8B3O+HDZQGH7Mooz97ddAe5BFLJxVvAnIPLPB0xb5J/PVfEz5myikMkHxmoyPKHypP/IOy7Vkn0iRmHTkFEfUZxS5dDLPKCS3F9Bkmn6xQAYilNs0xMp1RZg6T2RPqpTqlJi0JWwX6LmFPJjBi2ioQHQlqswxvOcBJGqV5IhVWzGoEX1sdCmWu0A==",
		"c79cd2df6aa9dc7ef7e4d07514a6c5c0276bd313469d749b0df74035d135b25bd3d1c69f63cd094fdeee8247c2bb557c");
}
