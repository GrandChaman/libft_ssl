/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hmac.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/28 14:47:34 by fle-roy           #+#    #+#             */
/*   Updated: 2020/03/01 18:18:27 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_hash.h"
#include "ft_ssl_hmac.h"

#define HMAC_IPAD 0x36
#define HMAC_OPAD 0x5c

void				ft_hmac_gen_pad(t_ft_hmac_ctx *ctx, uint8_t *buf,
	uint8_t c, size_t len)
{
	size_t i;

	i = -1;
	while (++i < len)
		buf[i] = ctx->key[i] ^ c;
}

void			*ft_hmac_destroy(t_ft_hmac_ctx *ctx)
{
	if (!ctx)
		return (NULL);
	free(ctx->key);
	free(ctx->ipad);
	free(ctx->opad);
	free(ctx);
	return (NULL);
}

t_ft_hmac_ctx		*ft_hmac_init(t_hash_algo algo, uint8_t *key, size_t klen)
{
	uint8_t			*nkey;
	t_ft_hmac_ctx	*ctx;

	ctx = ft_memalloc(sizeof(t_ft_hmac_ctx));
	nkey = ft_memalloc(g_hash_table[algo].block_size);
	if (!ctx || !nkey)
		return (ft_hmac_destroy(ctx));
	ft_bzero(nkey, g_hash_table[algo].block_size);
	if (klen > g_hash_table[algo].block_size)
		key = ft_hash_exec(algo, key, klen, HASH_RAW);
	ft_memcpy(nkey, key, (klen > g_hash_table[algo].block_size
		? g_hash_table[algo].out_len : klen));
	if (klen > g_hash_table[algo].block_size)
		free(key);
	ctx->key = nkey;
	ctx->klen = klen;
	ctx->ipad = ft_memalloc(g_hash_table[algo].block_size);
	ctx->opad = ft_memalloc(g_hash_table[algo].block_size);
	ctx->hash = algo;
	if (!ctx->ipad || !ctx->opad)
		return (ft_hmac_destroy(ctx));
	ft_hmac_gen_pad(ctx, ctx->ipad, HMAC_IPAD, g_hash_table[algo].block_size);
	ft_hmac_gen_pad(ctx, ctx->opad, HMAC_OPAD, g_hash_table[algo].block_size);
	return (ctx);
}

uint8_t			*ft_hmac(t_ft_hmac_ctx *ctx, uint8_t *msg, size_t mlen,
	uint8_t opt)
{
	uint8_t *inner_buf;
	uint8_t *outer_buf;
	uint8_t *res;

	inner_buf = ft_memalloc(g_hash_table[ctx->hash].block_size + mlen);
	outer_buf = ft_memalloc(g_hash_table[ctx->hash].block_size
		+ g_hash_table[ctx->hash].out_len);
	if (!inner_buf || !outer_buf)
		return (NULL);
	ft_memcpy(inner_buf, ctx->ipad, g_hash_table[ctx->hash].block_size);
	ft_memcpy(inner_buf + g_hash_table[ctx->hash].block_size, msg, mlen);
	ft_memcpy(outer_buf, ctx->opad, g_hash_table[ctx->hash].block_size);
	res = ft_hash_exec(ctx->hash, inner_buf,
		g_hash_table[ctx->hash].block_size + mlen, HASH_RAW);
	if (!res)
		return (NULL);
	ft_memcpy(outer_buf + g_hash_table[ctx->hash].block_size, res,
		g_hash_table[ctx->hash].out_len);
	free(res);
	res = ft_hash_exec(ctx->hash, outer_buf, g_hash_table[ctx->hash].block_size
		+ g_hash_table[ctx->hash].out_len, opt);
	return (res);
}
