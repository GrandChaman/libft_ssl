/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   base64_decode.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/29 10:58:27 by fle-roy           #+#    #+#             */
/*   Updated: 2019/06/30 11:59:25 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "ft_ssl_base64.h"

void		base64_decode(uint8_t *dest, uint8_t *src, size_t *i, size_t *j)
{
	dest[0] = g_b64_decode_table[src[0]] << 2 | g_b64_decode_table[src[1]] >> 4;
	dest[1] = g_b64_decode_table[src[1]] << 4 | g_b64_decode_table[src[2]] >> 2;
	dest[2] = g_b64_decode_table[src[2]] << 6 | g_b64_decode_table[src[3]];
	if (i)
		*i += 4;
	if (j)
	{
		*j += 3;
		if (src[3] == '=')
			*j -= 1;
		if (src[2] == '=')
			*j -= 1;
	}
}

size_t		ft_base64_decode_update_prepare(t_ft_base64 *b64, char *str,
	size_t len, uint8_t *buf)
{
	size_t nlen;

	ft_bzero(buf, READ_BLOCK_AL + 3);
	nlen = (len > READ_BLOCK_AL ? READ_BLOCK_AL : len);
	ft_memcpy(buf, b64->dangling, b64->dangling_len);
	ft_memcpy(buf + b64->dangling_len, str, nlen);
	nlen += b64->dangling_len;
	ft_base64_remove_whitespace((char*)buf, &nlen);
	b64->dangling_len = nlen % 4;
	return (nlen);
}

char		ft_base64_decode_update(t_ft_base64 *b64, char *str, size_t len)
{
	size_t	i;
	size_t	offset;
	uint8_t	buf[READ_BLOCK_AL + 3];
	uint8_t	tmp[3];
	size_t	nlen;

	i = 0;
	ft_bzero(tmp, 3);
	nlen = ft_base64_decode_update_prepare(b64, str, len, buf);
	while (i < nlen - b64->dangling_len)
	{
		offset = 0;
		base64_decode(tmp, buf + i, &i, &offset);
		dbuf_append_w_len(&b64->obuf, (char*)tmp, offset);
	}
	ft_bzero(tmp, 3);
	ft_memcpy(b64->dangling, buf + nlen - b64->dangling_len, b64->dangling_len);
	if (len <= READ_BLOCK_AL)
		return (b64->out_fd >= 0 ? ft_base64_write_fd(b64) : 0);
	return (ft_base64_decode_update(b64, str + READ_BLOCK_AL,
		len - READ_BLOCK_AL));
}

char		*ft_base64_decode_finish(t_ft_base64 *base64)
{
	uint8_t	tmp_in[4];
	uint8_t	tmp_out[3];
	size_t	offset;

	ft_bzero(tmp_in, 4);
	ft_bzero(tmp_out, 3);
	if (base64->dangling_len)
	{
		offset = 0;
		ft_memcpy(tmp_in, base64->dangling, base64->dangling_len);
		base64_decode(tmp_out, tmp_in, NULL, &offset);
		dbuf_append_w_len(&base64->obuf, (char*)tmp_out, offset);
		ft_bzero(base64->dangling, 3);
		base64->dangling_len = 0;
	}
	if (base64->out_fd >= 0)
		return (NULL);
	return (base64->obuf.buf);
}
