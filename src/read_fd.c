/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_fd.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/10 14:31:26 by fle-roy           #+#    #+#             */
/*   Updated: 2020/02/28 13:32:30 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>

char		ft_read_fd(int fd, void *param,
	char (*update)(void*, char*, size_t), uint8_t opt)
{
	char	buf[READ_BLOCK_AL + 1];
	ssize_t	nb_read;
	ssize_t	nb_read_tot;
	char	ret;

	ft_bzero(buf, READ_BLOCK_AL + 1);
	nb_read = 0;
	nb_read_tot = 0;
	while ((nb_read = read(fd, buf + nb_read_tot % READ_BLOCK_AL,
		READ_BLOCK_AL - nb_read_tot % READ_BLOCK_AL)) > 0)
	{
		nb_read_tot += nb_read;
		if (opt & FT_READ_ECHO)
			ft_printf("%s", buf);
		if ((ret = update(param, buf, (size_t)nb_read_tot)))
			return (ret);
		ft_bzero(buf, nb_read);
		if (nb_read_tot >= READ_BLOCK_AL && opt & FT_READ_SINGLE_ROUND)
			break ;
		nb_read_tot = 0;
	}
	if (nb_read_tot && !nb_read)
		return (update(param, buf, (size_t)nb_read_tot));
	return (nb_read < 0 ? 1 : 0);
}
