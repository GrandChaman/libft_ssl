%macro	aes128_encrypt_logic 0
	pxor xmm15, xmm0
	aesenc xmm15, xmm1
	aesenc xmm15, xmm2
	aesenc xmm15, xmm3
	aesenc xmm15, xmm4
	aesenc xmm15, xmm5
	aesenc xmm15, xmm6
	aesenc xmm15, xmm7
	aesenc xmm15, xmm8
	aesenc xmm15, xmm9
	aesenclast xmm15, xmm10
%endmacro

%macro	aes128_decrypt_logic 0
	pxor xmm15, xmm10
	aesdec xmm15, xmm9
	aesdec xmm15, xmm8
	aesdec xmm15, xmm7
	aesdec xmm15, xmm6
	aesdec xmm15, xmm5
	aesdec xmm15, xmm4
	aesdec xmm15, xmm3
	aesdec xmm15, xmm2
	aesdec xmm15, xmm1
	aesdeclast xmm15, xmm0
%endmacro
section .text
	global aes128_encrypt_block
	global aes128_decrypt_block
	global aes128_encrypt_ecb
	global aes128_encrypt_cbc
	global aes128_decrypt_ecb
	global aes128_decrypt_cbc

aes128_encrypt_cbc:
	push rdi
	push rcx
	mov rcx, rdi
	add rcx, rsi
	movdqu xmm15, OWORD [rdi]
	pxor xmm15, [rdx]
	jmp .post_xor
.begin:
	pxor xmm15, [rdi]
.post_xor:
	aes128_encrypt_logic
	movdqu OWORD [rdi], xmm15
	add rdi, 0x10
	cmp rdi, rcx
	jl .begin
	pop rcx
	pop rdi
	ret

aes128_decrypt_cbc:
	push rcx
	mov rcx, rdi
	add rcx, rsi
	sub rcx, 0x10
.begin:
	movdqu xmm15, OWORD [rcx]
	aes128_decrypt_logic
	sub rcx, 0x10
	cmp rcx, rdi
	jl .iv
	movdqu xmm14, OWORD [rcx]
	pxor xmm15, xmm14
	movdqu OWORD [rcx+0x10], xmm15
	jmp .begin
.iv:
	movdqu xmm14, OWORD [rdx]
	pxor xmm15, xmm14
	movdqu OWORD [rdi], xmm15
	pop rcx
	ret

aes128_encrypt_ecb:
	push rdi
	push rcx
	mov rcx, rdi
	add rcx, rsi
.begin:
	movdqu xmm15, OWORD [rdi]
	aes128_encrypt_logic
	movdqu OWORD [rdi], xmm15
	add rdi, 0x10
	cmp rdi, rcx
	jl .begin
	pop rcx
	pop rdi
	ret

aes128_decrypt_ecb:
	push rdi
	push rcx
	mov rcx, rdi
	add rcx, rsi
.begin:
	movdqu xmm15, OWORD [rdi]
	aes128_decrypt_logic
	movdqu OWORD [rdi], xmm15
	add rdi, 0x10
	cmp rdi, rcx
	jl .begin
	pop rcx
	pop rdi
	ret

aes128_encrypt_block:
	movdqu xmm15, OWORD [rdi]
	aes128_encrypt_logic
	movdqu OWORD [rdi], xmm15
	ret

aes128_decrypt_block:
	movdqu xmm15, OWORD [rdi]
	aes128_decrypt_logic
	movdqu OWORD [rdi], xmm15
	ret
