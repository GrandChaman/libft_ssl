section .text
	global aes128_skey_imc
	global aes128_load_skey
	global aes128_gen_skey
	global aes128_save_skey
	global aes128_reverse_key
aes128_skey_imc:
	aesimc xmm1, xmm1
	aesimc xmm2, xmm2
	aesimc xmm3, xmm3
	aesimc xmm4, xmm4
	aesimc xmm5, xmm5
	aesimc xmm6, xmm6
	aesimc xmm7, xmm7
	aesimc xmm8, xmm8
	aesimc xmm9, xmm9
	ret
aes128_load_skey:
	movdqu xmm0, OWORD [rdi]
	movdqu xmm1, OWORD [rdi+0x10]
	movdqu xmm2, OWORD [rdi+0x20]
	movdqu xmm3, OWORD [rdi+0x30]
	movdqu xmm4, OWORD [rdi+0x40]
	movdqu xmm5, OWORD [rdi+0x50]
	movdqu xmm6, OWORD [rdi+0x60]
	movdqu xmm7, OWORD [rdi+0x70]
	movdqu xmm8, OWORD [rdi+0x80]
	movdqu xmm9, OWORD [rdi+0x90]
	movdqu xmm10, OWORD [rdi+0xA0]
	ret

aes128_save_skey:
	movdqu OWORD [rdi], xmm0
	movdqu OWORD [rdi+0x10], xmm1
	movdqu OWORD [rdi+0x20], xmm2
	movdqu OWORD [rdi+0x30], xmm3
	movdqu OWORD [rdi+0x40], xmm4
	movdqu OWORD [rdi+0x50], xmm5
	movdqu OWORD [rdi+0x60], xmm6
	movdqu OWORD [rdi+0x70], xmm7
	movdqu OWORD [rdi+0x80], xmm8
	movdqu OWORD [rdi+0x90], xmm9
	movdqu OWORD [rdi+0xA0], xmm10
	ret

aes128_gen_skey:
	movdqu xmm1, OWORD [rdi]
	movdqu OWORD [rsi], xmm1
	lea rcx, [rsi+0x10]
	aeskeygenassist xmm2, xmm1, 0x1
	call aes_key_expansion128
	aeskeygenassist xmm2, xmm1, 0x2
	call aes_key_expansion128
	aeskeygenassist xmm2, xmm1, 0x4
	call aes_key_expansion128
	aeskeygenassist xmm2, xmm1, 0x8
	call aes_key_expansion128
	aeskeygenassist xmm2, xmm1, 0x10
	call aes_key_expansion128
	aeskeygenassist xmm2, xmm1, 0x20
	call aes_key_expansion128
	aeskeygenassist xmm2, xmm1, 0x40
	call aes_key_expansion128
	aeskeygenassist xmm2, xmm1, 0x80
	call aes_key_expansion128
	aeskeygenassist xmm2, xmm1, 0x1b
	call aes_key_expansion128
	aeskeygenassist xmm2, xmm1, 0x36
	call aes_key_expansion128
	ret
aes_key_expansion128:
	pshufd xmm2, xmm2, 0xff
	vpslldq xmm3, xmm1, 0x4
	pxor xmm1, xmm3
	vpslldq xmm3, xmm1, 0x4
	pxor xmm1, xmm3
	vpslldq xmm3, xmm1, 0x4
	pxor xmm1, xmm3
	pxor xmm1, xmm2
	movdqu OWORD [rcx], xmm1
	add rcx, 0x10
	ret

aes128_reverse_key:
	enter 16, 0
	push rax
	movdqu xmm0, OWORD [rdi]
	mov rax, 0x0001020304050607
	mov QWORD [rsp], rax
	mov rax, 0x08090a0b0c0d0e0f
	mov QWORD [rsp+8], rax
	movlps xmm1, QWORD [rsp+8]
	movhps xmm1, QWORD [rsp]
	pshufb xmm0, xmm1
	movdqu OWORD [rdi], xmm0
	pop rax
	leave
	ret
