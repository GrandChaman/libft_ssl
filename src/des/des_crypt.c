/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   des_crypt.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/29 10:58:27 by fle-roy           #+#    #+#             */
/*   Updated: 2019/07/01 10:10:53 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "ft_ssl_des.h"

char	ft_des_routine(t_ft_des *des, uint8_t last)
{
	ssize_t		i;
	ssize_t		inc;
	ssize_t		usable_len;
	char		tmp[8];

	i = 0;
	if (last)
		usable_len = des->ibuf->cursor;
	else if (des->ibuf->cursor && !des->ibuf->cursor % 8)
		usable_len = des->ibuf->cursor - 8;
	else if (des->ibuf->cursor)
		usable_len = des->ibuf->cursor - des->ibuf->cursor % 8;
	else
		return (0);
	while (i < usable_len)
	{
		inc = (des->cipher_opt & DES_NO_PAD &&
			usable_len - i < 8 ? usable_len - i : 8);
		des->routine(des, des->ibuf->buf, tmp);
		dbuf_pop_back(des->ibuf, inc);
		dbuf_append_w_len(des->obuf, tmp, inc);
		i += 8;
	}
	des->size += i;
	return (0);
}

char	ft_des_finish(t_ft_des *des)
{
	char	ret;

	if (des->armored && des->mode == DES_OPT_D)
		ft_base64_decode_finish(&des->base64);
	if (des->mode == DES_OPT_D)
		ft_des_decrypt_handle_padding(des);
	else
		ft_des_finish_enc(des);
	if (des->armored && des->mode == DES_OPT_E)
	{
		ft_base64_encode_update(&des->base64, des->obuf->buf,
			des->obuf->cursor);
		ft_base64_encode_finish(&des->base64);
		ret = ft_base64_write_fd(&des->base64);
		if (!des->base64.width || des->base64.cwrote % des->base64.width)
			ret += write(des->fd_out, "\n", 1) >= 0 ? 0 : 1;
		return (ret);
	}
	return (ft_des_print(des, 1));
}
