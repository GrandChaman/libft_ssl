/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   des_decrypt.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/29 10:58:27 by fle-roy           #+#    #+#             */
/*   Updated: 2019/06/30 12:31:32 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "ft_ssl_des.h"

void	ft_des_decrypt_handle_padding(t_ft_des *des)
{
	uint8_t	last;
	uint8_t	i;

	i = 1;
	ft_des_routine(des, 1);
	if (des->cipher_opt & DES_NO_PAD)
		return ;
	last = des->obuf->buf[des->obuf->cursor - 1];
	while (i < 8 && last == des->obuf->buf[des->obuf->cursor - (i + 1)])
		i++;
	dbuf_pop_front(des->obuf, i);
}
