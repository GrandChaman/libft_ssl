/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   des.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/29 10:58:27 by fle-roy           #+#    #+#             */
/*   Updated: 2020/02/28 13:52:58 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "ft_ssl_des.h"
#include <pwd.h>

t_ft_des_block_cipher_mode	g_des_block_cipher_mode[] =
{
	{"des", &ft_des_routine_cbc, DES_IV},
	{"des-ecb", &ft_des_routine_ecb, 0},
	{"des-cbc", &ft_des_routine_cbc, DES_IV},
	{"des-ofb", &ft_des_routine_ofb, DES_IV | DES_NO_PAD | DES_SYM},
	{"des-cfb", &ft_des_routine_cfb, DES_IV | DES_NO_PAD | DES_SYM},
	{NULL, NULL, 0}
};

