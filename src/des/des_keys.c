/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   des_keys.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/04 16:02:32 by fle-roy           #+#    #+#             */
/*   Updated: 2019/07/01 09:15:39 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "ft_ssl_des.h"

uint8_t		g_des_pc1[56] = {
	57, 49, 41, 33, 25, 17, 9, 1,
	58, 50, 42, 34, 26, 18, 10, 2,
	59, 51, 43, 35, 27, 19, 11, 3,
	60, 52, 44, 36, 63, 55, 47,
	39, 31, 23, 15, 7, 62, 54, 46,
	38, 30, 22, 14, 6, 61, 53, 45, 37,
	29, 21, 13, 5, 28, 20, 12, 4
};

uint8_t		g_des_subkey_ls[16] = {
	1, 1, 2, 2, 2, 2, 2,
	2, 1, 2, 2, 2, 2, 2,
	2, 1
};

uint8_t		g_des_pc2[48] = {
	14, 17, 11, 24, 1, 5,
	3, 28, 15, 6, 21, 10,
	23, 19, 12, 4, 26, 8,
	16, 7, 27, 20, 13, 2,
	41, 52, 31, 37, 47,
	55, 30, 40, 51, 45,
	33, 48, 44, 49, 39,
	56, 34, 53, 46, 42,
	50, 36, 29, 32
};

void		ft_des_subkey_expansion(t_ft_couple_num initial_subkey,
	t_ft_couple_num res[16])
{
	uint8_t		i;

	i = 0;
	res[0].left.x28 = (unsigned int)(((initial_subkey.left.x28)
		<< (g_des_subkey_ls[0]))
		| ((initial_subkey.left.x28) >> (28 - (g_des_subkey_ls[0]))));
	res[0].right.x28 = (unsigned int)(((initial_subkey.right.x28)
		<< (g_des_subkey_ls[0]))
		| ((initial_subkey.right.x28) >> (28 - (g_des_subkey_ls[0]))));
	while (++i < 16)
	{
		res[i].left.x28 = (unsigned int)(((res[i - 1].left.x28)
		<< (g_des_subkey_ls[i]))
		| ((res[i - 1].left.x28) >> (28 - (g_des_subkey_ls[i]))));
		res[i].right.x28 = (unsigned int)(((res[i - 1].right.x28)
		<< (g_des_subkey_ls[i]))
		| ((res[i - 1].right.x28) >> (28 - (g_des_subkey_ls[i]))));
	}
}

t_ft_num	ft_des_pc1(t_ft_num key)
{
	t_ft_num	res;
	uint8_t		i;

	res.x64 = 0;
	i = -1;
	while (++i < 56)
		res.x56 |= (unsigned long)(((key.x64 & ((unsigned long)1
			<< ((64 - g_des_pc1[i])))) ? 1 : 0)) << (55 - i);
	return (res);
}

void		ft_des_pc2(t_ft_couple_num subkey_pre_pc2[16],
	t_ft_num res[16])
{
	t_ft_num	tmp;
	uint8_t		i;
	uint8_t		ii;

	ii = -1;
	while (++ii < 16)
	{
		i = -1;
		res[ii].x64 = 0;
		tmp.x56 = (((unsigned long)subkey_pre_pc2[ii].left.x28 << 28)
			| ((unsigned long)subkey_pre_pc2[ii].right.x28));
		while (++i < 48)
			res[ii].x48 |= (unsigned long)(((tmp.x56 & ((unsigned long)1
				<< ((56 - g_des_pc2[i])))) ? 1 : 0)) << (47 - i);
	}
}

void		ft_des_get_subkeys(t_ft_num key, t_ft_num res[16])
{
	t_ft_num		pc1;
	t_ft_couple_num	initial_subkey;
	t_ft_couple_num	sub_keys[16];

	pc1 = ft_des_pc1(key);
	ft_des_split_56(pc1, &initial_subkey);
	ft_des_subkey_expansion(initial_subkey, sub_keys);
	ft_des_pc2(sub_keys, res);
}

t_ft_num	ft_des_str_to_key(char *key)
{
	t_ft_num	res;
	size_t		len;
	size_t		i;

	len = ft_strlen(key);
	res.x128 = 0;
	i = 0;
	while (i < 8 && i < len)
	{
		res.x64 |= (unsigned long)key[i] << (64 - (i + 1) * 8);
		i++;
	}
	while (i < 8)
	{
		res.x64 |= (unsigned long)0 << (64 - (i + 1) * 8);
		i++;
	}
	return (res);
}
