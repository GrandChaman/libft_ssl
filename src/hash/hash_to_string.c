/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash_to_string.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/29 15:27:58 by fle-roy           #+#    #+#             */
/*   Updated: 2020/02/28 16:48:17 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

uint8_t		*ft_hash_to_raw(t_ft_hash_ctx *ctx, uint8_t *res, uint8_t rev)
{
	size_t	i;

	i = -1;
	while (++i < ctx->algo->out_reg)
	{
		if (!rev)
			ft_memrcpy(res + i * ctx->algo->len_size / 2,
				ctx->registers[i].c, ctx->algo->len_size / 2);
		else
			ft_memcpy(res + i * ctx->algo->len_size / 2,
				ctx->registers[i].c, ctx->algo->len_size / 2);
	}
	ft_hash_destroy(ctx);
	return (res);
}

uint8_t		*ft_hash_to_ascii(t_ft_hash_ctx *ctx, uint8_t *res, uint8_t rev)
{
	size_t	i;

	i = -1;
	while (++i < ctx->algo->out_reg)
	{
		if (ctx->algo->len_size > 8)
			ft_snprintf((char*)res + i *  ctx->algo->len_size, ctx->algo->len_size,
				"%016llx", rev ? brev_64(ctx->registers[i].x64)
					: ctx->registers[i].x64);
		else
			ft_snprintf((char*)res + i * ctx->algo->len_size, ctx->algo->len_size,
				"%08x", rev ? brev_32(ctx->registers[i].x32)
					: ctx->registers[i].x32);
	}
	ft_hash_destroy(ctx);
	return (res);
}

uint8_t		*ft_hash_to_string(t_ft_hash_ctx *ctx, uint8_t rev)
{
	size_t	i;
	uint8_t	*res;

	i = -1;
	if (!(res = (uint8_t*)ft_strnew(ctx->algo->out_reg
		* ctx->algo->len_size * sizeof(char))))
		return (NULL);
	if (ctx->opt & HASH_RAW)
		return (ft_hash_to_raw(ctx, res, rev));
	else
		return (ft_hash_to_ascii(ctx, res, rev));
}
