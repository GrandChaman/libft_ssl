/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/29 16:35:43 by fle-roy           #+#    #+#             */
/*   Updated: 2020/02/28 18:36:44 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "ft_ssl_hash.h"

t_ft_hash		g_hash_table[] = {
	{"MD5", MD5_REG_NB, HASH_BLOCK_512, HASH_LEN_X64, HASH_MD5_LEN, MD5_REG_NB, &ft_md5_init, &ft_md5_update,
		&ft_md5_finish},
	{"SHA1", SHA1_REG_NB, HASH_BLOCK_512, HASH_LEN_X64, HASH_SHA1_LEN, SHA1_REG_NB, &ft_sha1_init, &ft_sha1_update,
		&ft_sha1_finish},
	{"SHA224", SHA2_REG_NB, HASH_BLOCK_512, HASH_LEN_X64, HASH_SHA224_LEN, SHA2_REG_NB - 1, &ft_sha224_init, &ft_sha2_update,
		&ft_sha224_finish},
	{"SHA256", SHA2_REG_NB, HASH_BLOCK_512, HASH_LEN_X64, HASH_SHA256_LEN, SHA2_REG_NB, &ft_sha256_init, &ft_sha2_update,
		&ft_sha256_finish},
	{"SHA384", SHA3_REG_NB, HASH_BLOCK_1024, HASH_LEN_X128, HASH_SHA384_LEN, SHA3_REG_NB - 2, &ft_sha384_init, &ft_sha3_update,
		&ft_sha384_finish},
	{"SHA512", SHA3_REG_NB, HASH_BLOCK_1024, HASH_LEN_X128, HASH_SHA512_LEN, SHA3_REG_NB, &ft_sha512_init, &ft_sha3_update,
		&ft_sha512_finish},
	{"SHA512224", SHA3_REG_NB, HASH_BLOCK_1024, HASH_LEN_X128, HASH_SHA224_LEN, SHA3_REG_NB, &ft_sha512224_init, &ft_sha3_update,
		&ft_sha512224_finish},
	{"SHA512256", SHA3_REG_NB, HASH_BLOCK_1024, HASH_LEN_X128, HASH_SHA256_LEN, SHA3_REG_NB, &ft_sha512256_init, &ft_sha3_update,
		&ft_sha512256_finish},
	{NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL}
};

uint8_t		*ft_hash_exec(size_t hash, uint8_t *str, size_t len, uint8_t opt)
{
	t_ft_hash_ctx	ctx;

	ctx.hash = g_hash_table[hash];
	ctx.hash.init(&ctx);
	ctx.opt = opt;
	ft_hash_block_update(&ctx, (unsigned char*)str,
		len ? len : ft_strlen((const char*)str));
	return (ft_hash_block_finish(&ctx));
}

void		ft_hash_destroy(t_ft_hash_ctx *ctx)
{
	size_t	i;

	i = 0;
	while (i < ctx->hash.reg_nb)
		ctx->registers[i++].x64 = 0;
	ft_memdel((void**)&ctx->registers);
	ft_bzero(ctx->buf, ctx->hash.block_size);
	ft_memdel((void**)&ctx->buf);
	ctx->cursor = 0;
	ctx->total_size.x128 = 0;
}

void		ft_hash_init(t_ft_hash_ctx *ctx, t_ft_hash *algo)
{
	ctx->algo = algo;
	ctx->registers = ft_memalloc(sizeof(t_ft_num) * ctx->hash.reg_nb);
	ctx->buf = ft_memalloc(sizeof(char) * ctx->hash.block_size);
	ft_bzero(ctx->buf, ctx->hash.block_size);
	ctx->cursor = 0;
	ctx->total_size.x128 = 0;
	ctx->opt = 0;
}
