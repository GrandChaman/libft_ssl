/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha384_512.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/13 15:12:11 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/22 11:05:31 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"

void	ft_sha3_routine(uint64_t reg[SHA3_REG_NB], uint64_t tmp[2])
{
	reg[REG_H] = reg[REG_G];
	reg[REG_G] = reg[REG_F];
	reg[REG_F] = reg[REG_E];
	reg[REG_E] = reg[REG_D] + tmp[0];
	reg[REG_D] = reg[REG_C];
	reg[REG_C] = reg[REG_B];
	reg[REG_B] = reg[REG_A];
	reg[REG_A] = tmp[0] + tmp[1];
}

void	ft_sha3_update(t_ft_hash_ctx *ctx, t_hash_block hb)
{
	size_t		i;
	uint64_t	w[80];
	uint64_t	reg[80];
	uint64_t	tmp[2];

	i = -1;
	while (++i < 16)
		w[i] = (!g_be ? brev_64(hb.bwords[i]) : hb.bwords[i]);
	i = 15;
	ft_hash_copy_reg_long(ctx, reg);
	while (++i < 80)
		w[i] = sha3_op_ssig1(w[i - 2]) + w[i - 7]
			+ sha3_op_ssig0(w[i - 15]) + w[i - 16];
	i = -1;
	while (++i < 80)
	{
		tmp[0] = reg[REG_H] + sha3_op_bsig1(reg[REG_E])
			+ sha3_op_ch(reg[REG_E], reg[REG_F], reg[REG_G])
			+ g_sha3_k[i] + w[i];
		tmp[1] = sha3_op_bsig0(reg[REG_A])
			+ sha3_op_maj(reg[REG_A], reg[REG_B], reg[REG_C]);
		ft_sha3_routine(reg, tmp);
	}
	add_reg_long(ctx, (uint64_t*)reg);
}
