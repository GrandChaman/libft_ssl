/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha384.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/08 17:40:09 by fle-roy           #+#    #+#             */
/*   Updated: 2020/02/28 16:46:52 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"

void	ft_sha384_init(t_ft_hash_ctx *ctx)
{
	ft_hash_init(ctx, &g_hash_table[HT_SHA384]);
	ctx->registers[REG_A].x64 = 0xCBBB9D5DC1059ED8;
	ctx->registers[REG_B].x64 = 0x629A292A367CD507;
	ctx->registers[REG_C].x64 = 0x9159015A3070DD17;
	ctx->registers[REG_D].x64 = 0x152FECD8F70E5939;
	ctx->registers[REG_E].x64 = 0x67332667FFC00B31;
	ctx->registers[REG_F].x64 = 0x8EB44A8768581511;
	ctx->registers[REG_G].x64 = 0xDB0C2E0D64F98FA7;
	ctx->registers[REG_H].x64 = 0x47B5481DBEFA4FA4;
}

uint8_t	*ft_sha384_finish(t_ft_hash_ctx *ctx, t_hash_block hb)
{
	hb.bwords[14] = !g_be ? brev_64(ctx->total_size.x128 >> 64)
		: ctx->total_size.x128 >> 64;
	hb.bwords[15] = !g_be ? brev_64((ctx->total_size.x128 << 64) >> 64)
		: (ctx->total_size.x128 << 64) >> 64;
	ctx->hash.update(ctx, hb);
	return (ft_hash_to_string(ctx, 0));
}
