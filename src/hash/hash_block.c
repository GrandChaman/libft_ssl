/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash_block.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/29 15:27:58 by fle-roy           #+#    #+#             */
/*   Updated: 2020/02/28 16:48:28 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

char		ft_hash_block_update(t_ft_hash_ctx *ctx, unsigned char *data,
	size_t size)
{
	t_hash_block	hb;
	size_t			inc;

	inc = size > ctx->hash.block_size - ctx->cursor
		? ctx->hash.block_size - ctx->cursor : size;
	ft_memcpy(ctx->buf + ctx->cursor, data, inc);
	ctx->cursor += inc;
	size -= inc;
	if (ctx->cursor >= ctx->hash.block_size)
	{
		ft_memcpy(hb.blocks, ctx->buf, ctx->hash.block_size);
		ctx->hash.update(ctx, hb);
		ctx->total_size.x128 += ctx->hash.block_size * 8;
		ctx->cursor = 0;
		ft_bzero(ctx->buf, ctx->hash.block_size);
	}
	if (size > 0)
		ft_hash_block_update(ctx, data + inc, size);
	return (0);
}

uint8_t		*ft_hash_block_finish(t_ft_hash_ctx *ctx)
{
	char			added_one;
	t_hash_block	hb;

	added_one = 0;
	ft_memcpy(hb.blocks, ctx->buf, ctx->hash.block_size);
	if (ctx->cursor >= ctx->hash.block_size - ctx->hash.len_size)
	{
		if (ctx->cursor < ctx->hash.block_size)
		{
			hb.blocks[ctx->cursor] = 128;
			added_one = 1;
		}
		ctx->hash.update(ctx, hb);
		ctx->total_size.x128 += ctx->cursor * 8;
		ctx->cursor = 0;
		ft_bzero(hb.blocks, ctx->hash.block_size);
		ft_bzero(ctx->buf, ctx->hash.block_size);
	}
	ctx->total_size.x128 += ctx->cursor * 8;
	if (!added_one)
		hb.blocks[ctx->cursor] = 128;
	return (ctx->hash.finish(ctx, hb));
}

void		ft_hash_copy_reg(t_ft_hash_ctx *ctx, uint32_t *reg)
{
	size_t	i;

	i = -1;
	while (++i < ctx->hash.reg_nb)
		reg[i] = ctx->registers[i].x32;
}

void		ft_hash_copy_reg_long(t_ft_hash_ctx *ctx, uint64_t *reg)
{
	size_t	i;

	i = -1;
	while (++i < ctx->hash.reg_nb)
		reg[i] = ctx->registers[i].x64;
}
