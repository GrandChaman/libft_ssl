/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/09 16:52:05 by fle-roy           #+#    #+#             */
/*   Updated: 2020/03/01 17:52:44 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"
#include <pwd.h>

char		is_be(void)
{
	int		test;
	char	*p;

	test = 1;
	p = (char*)&test;
	return (p[0] == 0);
}

void		ft_xor(uint8_t *s1, uint8_t *s2, size_t len)
{
	size_t i;

	i = -1;
	while (++i < len)
		s1[i] ^= s2[i];
}

char		*ft_ssl_get_pass(uint8_t repeat)
{
	char *first;
	char *second;

	first = getpass("Please enter your password:");
	if (repeat)
	{
		second = ft_strnew(_PASSWORD_LEN);
		ft_memccpy(second, first, '\0', _PASSWORD_LEN);
		first = second;
		second = getpass("Please repeat your password:");
		if (ft_strcmp(first, second))
		{
			ft_strdel(&first);
			ft_bzero(second, _PASSWORD_LEN);
			ft_fprintf(2, "Passwords don't match.\n");
			return (NULL);
		}
		ft_strdel(&first);
		return (second);
	}
	return (first);
}

uint64_t	ft_pad_hex(char *str)
{
	size_t		key_len;
	char		tmp[17];

	key_len = ft_strlen(str);
	if (key_len > 16)
		key_len = 16;
	ft_strncpy(tmp, str, 16);
	ft_memset(tmp + key_len, '0', 16 - key_len);
	tmp[16] = 0;
	return (ft_atoi_hex(tmp));
}
