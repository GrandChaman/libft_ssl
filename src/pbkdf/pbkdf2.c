/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pbkdf2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/01 15:29:24 by fle-roy           #+#    #+#             */
/*   Updated: 2020/03/02 00:13:56 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#define PKFDF2_HEX_CHARSET "0123456789abcdef"

void				*ft_pbkdf2_destroy(t_ft_pbkdf2_ctx *ctx)
{
	if (ctx)
	{
		free(ctx->pass);
		free(ctx->salt);
	}
	free(ctx);
	return (NULL);
}

t_ft_pbkdf2_ctx		*ft_pbkdf2_init(t_hash_algo	algo, uint32_t c, char opt)
{
	t_ft_pbkdf2_ctx	*res;

	res = ft_memalloc(sizeof(t_ft_pbkdf2_ctx));
	if (!res)
		return (NULL);
	res->algo = algo;
	res->c = c;
	res->opt = opt;
	return (res);
}

uint8_t				*ft_pbkdf2_u(t_ft_pbkdf2_ctx *ctx, t_ft_hmac_ctx *prf,
	uint32_t i)
{
	uint8_t			*tmp;
	uint8_t			*buf;
	uint32_t		ii;

	ii = 0;
	tmp = NULL;
	ft_memcpy(ctx->buf, ctx->salt, ctx->slen);
	ft_pbkdf2_set_nonce(ctx->buf + ctx->slen, i);
	if (!(buf = ft_hmac(prf, ctx->buf, ctx->slen
		+ sizeof(uint32_t), HASH_RAW)))
		return (NULL);
	ft_bzero(ctx->buf, ctx->slen + sizeof(uint32_t));
	ft_memcpy(ctx->buf, buf, g_hash_table[ctx->algo].out_len);
	while (++ii < ctx->c)
	{
		if (!(tmp = ft_hmac(prf, buf,
			g_hash_table[ctx->algo].out_len, HASH_RAW)))
		return (NULL);
		free(buf);
		buf = tmp;
		ft_xor(ctx->buf, buf, g_hash_table[ctx->algo].out_len);
	}
	free(buf);
	return (ctx->buf);
}

uint8_t				*ft_pbkdf2(t_ft_pbkdf2_ctx *ctx, uint32_t dklen)
{
	uint32_t		i;
	uint32_t		tn;
	uint8_t			*res;
	uint8_t			*tmp;
	t_ft_hmac_ctx	*prf;

	ctx->blen = (ctx->slen + 4 > g_hash_table[ctx->algo].out_len
		? ctx->slen + 4 : g_hash_table[ctx->algo].out_len);
	tn = dklen / g_hash_table[ctx->algo].out_len +
		(dklen % g_hash_table[ctx->algo].out_len ? 1 : 0);
	ctx->buf = ft_memalloc(ctx->blen);
	prf = ft_hmac_init(ctx->algo, ctx->pass, ctx->plen);
	res = ft_memalloc(tn * g_hash_table[ctx->algo].out_len);
	if (!prf || !ctx->buf || !res)
		return (NULL);
	i = 1;
	while (i <= tn)
	{
		tmp = ft_pbkdf2_u(ctx, prf, i);
		ft_memcpy(res + (i++ - 1) * g_hash_table[ctx->algo].out_len, tmp,
			g_hash_table[ctx->algo].out_len);
		free(tmp);
	}
	// free(ctx->buf);
	ft_bzero(res + dklen, tn * g_hash_table[ctx->algo].out_len - dklen);
	return (ctx->opt & HASH_RAW ? res : (uint8_t*)ft_pbkdf2_hex(dklen, res));
}
