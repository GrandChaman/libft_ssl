/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pbkdf2_utils.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/02 00:13:05 by fle-roy           #+#    #+#             */
/*   Updated: 2020/03/02 00:13:45 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#define PKFDF2_HEX_CHARSET "0123456789abcdef"

char				*ft_pbkdf2_hex(uint32_t dklen, uint8_t *buf)
{
	char	*res;
	size_t	i;
	
	res = ft_strnew(dklen * 2);
	i = -1;
	while (++i < dklen)
	{
		res[i * 2] = PKFDF2_HEX_CHARSET[(buf[i] / 16) % 16];
		res[i * 2 + 1] = PKFDF2_HEX_CHARSET[buf[i] % 16];
	}
	free(buf);
	return (res);
}

char				ft_pbkdf2_set_pass(t_ft_pbkdf2_ctx *ctx,
	uint8_t *pass, size_t len)
{
	if (ctx->pass)
		free(ctx->pass);
	ctx->pass = ft_memalloc(len);
	ctx->plen = len;
	if (!ctx->pass)
		return (ft_pbkdf2_destroy(ctx) || 1);
	ft_memcpy(ctx->pass, pass, len);
	return (0);
}

char				ft_pbkdf2_set_salt(t_ft_pbkdf2_ctx *ctx,
	uint8_t *salt, size_t len)
{
	if (ctx->salt)
		free(ctx->salt);
	ctx->salt = ft_memalloc(len);
	ctx->slen = len;
	if (!ctx->salt)
		return (ft_pbkdf2_destroy(ctx) || 1);
	ft_memcpy(ctx->salt, salt, len);
	return (0);
}

void				ft_pbkdf2_set_nonce(uint8_t *buf,
	uint32_t nonce)
{
	if (!buf)
		return ;
	if (g_be)
		ft_memcpy(buf, (uint8_t*)&nonce, 4);
	else
		ft_memrcpy(buf, (uint8_t*)&nonce, 4);
}
